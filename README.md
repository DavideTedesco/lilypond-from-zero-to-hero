# LilyPond from Zero to Hero

## IT

### Part 1
- [Preview](http://lilypond.org/text-input.it.html)
- [Intro](https://github.com/SMERM/INTRO_LILYPOND/tree/master)
- Downloads
  - [LilyPond](http://lilypond.org/download.it.html)
  - [Frescobaldi](https://frescobaldi.org/download)
- [Manual](http://lilypond.org/learning.it.html)
- [Structure of the LilyPond file](https://lilypond.org/doc/v2.23/Documentation/learning/introduction-to-the-lilypond-file-structure)
- Examples
- Some sources to start
  - [LSR - The LilyPond Snippet Repository ♪♫](https://lsr.di.unimi.it/LSR/)
  - [LilyPond Snippets](https://lilypond.org/doc/v2.23/Documentation/snippets/index)
  - [openLilyLib/snippets](https://github.com/openlilylib/snippets)

## Useful Shortcuts for Frescobaldi

- `ctrl+shift+v` or `cmd+shift+v` to easily add the version of LilyPond in Frescobaldi
- `ctrl+m` or `cmd+m` to easily compile the music sheet in Frescobaldi
- `ctrl+s` or `cmd+s` to save the file
